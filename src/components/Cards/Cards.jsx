import React from "react";
import { Card, CardContent, Typography, Grid } from "@material-ui/core";
import CountUp from "react-countup";
import cx from "classnames";
import styles from './Cards.module.css';


const Cards = ({data: {confirmed, recovered, deaths, lastUpdate}, name}) => {
    if(!confirmed){
        return "Carregando dados..."
    };

    return (
        <div className={styles.container}>
            <Grid container spacing={3} justify="center">
                <Grid item xs={12} md={12} className={styles.center}>
                    <Typography variant="h3" className={styles.centered}>{name}</Typography>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.confirmed)}>
                    <CardContent>
                        <Typography variant="h4" align="center" color="textPrimary" gutterBottom>INFECTADOS</Typography>
                        <Typography align="center"  variant="h5">
                            <CountUp start={0} end={confirmed.value} duration={3} separator="." />
                        </Typography>
                        <Typography align="center"  color="textSecondary">Última atualização: { new Date(lastUpdate).toLocaleDateString('pt-BR') }</Typography>
                        <Typography align="center"  variant="body2">Números de casos ativos de COVID-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.recovered)}>
                    <CardContent>
                        <Typography variant="h4" align="center"  color="textPrimary" gutterBottom>RECUPERADOS</Typography>
                        <Typography align="center"  variant="h5">
                            <CountUp start={0} end={recovered.value} duration={3} separator="." />
                        </Typography>
                        <Typography align="center"  color="textSecondary">Última atualização: { new Date(lastUpdate).toLocaleDateString('pt-BR') }</Typography>
                        <Typography align="center"  variant="body2">Números de casos recuperados de COVID-19</Typography>
                    </CardContent>
                </Grid>
                <Grid item component={Card} xs={12} md={3} className={cx(styles.card, styles.deaths)}>
                    <CardContent>
                        <Typography variant="h4" align="center"  color="textPrimary" gutterBottom>MORTES</Typography>
                        <Typography align="center"  variant="h5">
                            <CountUp start={0} end={deaths.value} duration={3} separator="."/>
                        </Typography>
                        <Typography align="center"  color="textSecondary">Última atualização: { new Date(lastUpdate).toLocaleDateString('pt-BR') }</Typography>
                        <Typography align="center"  variant="body2">Números de vítimas fatais por COVID-19</Typography>
                    </CardContent>
                </Grid>
            </Grid>
        </div>
    )
}

export default Cards;