import React, { useState, useEffect } from "react";
import { NativeSelect, FormControl, Typography } from '@material-ui/core';
import { fetchCountries } from '../../api';

import styles from './CountryPicker.module.css';

const CountryPicker = ( { handlePlaceChenge }) => {
    const [fetchedCountries, setFetchedCountries] = useState([]);

    useEffect(() => {
        const fetchAPI = async () =>{
            setFetchedCountries(await fetchCountries())
        }
        fetchAPI();
    }, [setFetchedCountries]);

    const handleSub = (iso3) => {
        var CountryName = fetchedCountries.filter( nation =>  nation.iso3 === iso3 );
        if(CountryName != ""){
            handlePlaceChenge(iso3, CountryName[0].nome);
        }else{
            handlePlaceChenge(iso3, "");
        }

   }

    return (
    <div>
        <Typography variant="h5" align="center"  color="textPrimary" gutterBottom>Escolha o país para visualizar os dados.</Typography>
        <FormControl className={styles.formControl}>
            <NativeSelect defaultValue="" onChange={(e) => {handleSub(e.target.value)}}>
                <option value="">Panorama Global</option>
                {fetchedCountries.map((country, i) => <option key ={i} value={country.iso3}>{country.nome}</option>)}
            </NativeSelect>
        </FormControl>
        </div>
    )
}

export default CountryPicker;