import React, { useState, useEffect } from "react";
import {fetchDataDaily} from '../../api';
import { Line, Bar } from 'react-chartjs-2';

import styles from './Charts.module.css';

const Chart = ({ data, place }) => {
    const [ dailyData, setDailyData ] = useState({});

    useEffect( () => {
        const fetchAPI = async () => {
            setDailyData(await fetchDataDaily());
        }
        fetchAPI();

    }, [] )

    const LineChart = (
        dailyData[0]
        ? (
            <Line data = {{
                    labels: dailyData.map(({date}) => date),
                    datasets: [{
                        data: dailyData.map(({confirmed}) => confirmed),
                        label: 'Infectados', 
                        borderColor: '#ff6f00',
                        backgroundColor: 'rgba(255, 111, 0,0.5)',
                        fill: true,
                    }, {
                        data: dailyData.map(({deaths}) => deaths),
                        label: 'Mortes', 
                        borderColor: '#b71c1c',
                        backgroundColor: 'rgba(183, 28, 28, 0.9)',
                        fill: true,
                    }],
                }}
            />) : null
    );

    const BarChar = (
        data.confirmed
        ? (
            <Bar 
                data={{
                    labels: ['Infectados', 'Recuperados', 'Mortes'],
                    datasets: [{
                        label: "Pessoas",
                        backgroundColor: [
                            '#ff6f00',
                            '#1b5e20',
                            '#b71c1c',
                        ],
                        data: [data.confirmed.value, data.recovered.value, data.deaths.value],
                    }],
                }}
                options={{
                    legend: {display: false},
                    title: { display: true, text:'Visão geral dos casos' }
                }}
            />
        ): null
    );

    return (
        <div className={styles.container}>
            {place ? BarChar : LineChart}
        </div>
    )
}

export default Chart;