import axios from 'axios';

const covidUrl = "https://covid19.mathdro.id/api";

export const fetchData = async (place) => {
    let changeableUrl = covidUrl;
    if(place){
        changeableUrl = covidUrl+'/countries/'+place; 
    }
    try {
        const {data: { confirmed, recovered, deaths, lastUpdate }} = await axios.get(changeableUrl);

        return { confirmed, recovered, deaths, lastUpdate };

    } catch (error) {
        console.log("esse país não existe");
        console.log(error);
    }

};



export const fetchDataDaily = async () => {
    try {
        const { data } = await axios.get(covidUrl+'/daily');
        
        const modifiedData = data.map((dailyData) => ({
            confirmed: dailyData.confirmed.total,
            deaths: dailyData.deaths.total,
            date: dailyData.reportDate,
        }))
        //console.log(modifiedData)
        return modifiedData;
    } catch (error) {
        console.log(error);
    }
};

export const fetchCountries = async () => {
    try {
        const { data: {countries} } = await axios.get('./data/countriesJson_ptBR.json');
        
        return countries.map((country) => ({
                nome: country.nome,
                iso3: country.iso3
        }));
        console.log(countries);
    } catch (error) {
        console.log(error);
    }
};