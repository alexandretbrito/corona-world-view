import React from 'react';
import {Cards, Chart, CountryPicker, Footer} from './components/index';
import {fetchData} from "./api";
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions   from '@material-ui/core/DialogActions';
import {Typography} from "@material-ui/core";
import logo from "./images/corona-logo.png";

import styles from './App.module.css';

class App extends React.Component {
  state= {
    data: {},
    place: '',
    nome: 'Panorama Global',
    the_open: false,
  };

  async componentDidMount() {
    const fetchedData = await fetchData();;
    this.setState({data: fetchedData});
  }

  handlePlaceChenge = async (place, nome) => {
    const fetchedData = await fetchData(place);
    console.log(nome)
    if (fetchedData) {
      if(nome === ""){
        nome = "Panorama Global";
      };
      this.setState({data: fetchedData, place: place, nome: nome})
      console.log(this.state);
    } else { 
      this.handleClickOpen();
    }
  };

  handleClickOpen = () => {
    this.setState({the_open: true})
  };

  handleClose = () => {
    this.setState({the_open: false})
  };


  render(){
    const { data, place, the_open, nome } = this.state;

    return (
      <div className={styles.container}>
        <img src={logo} className={styles.logo}/>
        <Cards data={data} name={nome} />
        <CountryPicker handlePlaceChenge={this.handlePlaceChenge} />
        <Chart data={data} place={place} />
        <Footer />
        
        <Dialog onClose={this.handleClose} aria-labelledby="customized-dialog-title" open={the_open}>
        <DialogTitle id="customized-dialog-title" onClose={this.handleClose}>
          Informações não encontrados
        </DialogTitle>
        <DialogContent>
          <Typography gutterBottom>
           As informações relativas ao país escolhido não constam no banco de dados. Por favor, tente um outro país
          </Typography>
        </DialogContent>
        <DialogActions>
          <Button onClick={this.handleClose}>
            OK
          </Button>
        </DialogActions>
      </Dialog>
      </div>

    );
  }
} 

export default App;